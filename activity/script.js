let users = `[
	{
		"firstname":"John",
		"lastname":"Doe",
		"email":"john.doe@gmail.com",
		"password":"johndoeisawes0me",
		"isAdmin":true,
		"mobileNumber":"09104567891"
	},

	{
		"firstname":"Baby",
		"lastname":"Love",
		"email":"bblovej@gmail.com",
		"password":"bbisaqueen",
		"isAdmin":false,
		"mobileNumber":"09185678910"
	}

]`;

let orders = `[
	{
		"userId":1,
		"transactionDate":"Jan 15, 2022",
		"status":"paid",
		"total":1000
	},

	{
		"userId":2,
		"transactionDate":"Jan 15, 2022",
		"status":"shipped",
		"total":1000
	}
]`;

let products = `[
	{
		"name":"Coca Cola",
		"description":"Cherry Coke - Limited Edition",
		"price":50,
		"stocks":500,
		"isActive":true,
		"sku":"cocacola-01162022"
	},

	{
		"name":"Monami",
		"description":"Candy from your Childhood",
		"price":15,
		"stocks":1000,
		"isActive":true,
		"sku":"Monami-011622"
	}
]`;

let orderProducts = `[
	{
		"orderId":1,
		"productId":1,
		"quantity":2,
		"price":50,
		"subTotal":100
	}
]`;

console.log(JSON.parse(products));
